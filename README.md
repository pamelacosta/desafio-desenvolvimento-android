# Desafio Desenvolvimento Android

Criar um aplicativo usando java ou kotlin que consuma uma api do seguinte endereço

*https://gdscartao.com.br/nicolau-kerpen?querycard=15420003466&pin=YMON5BHOQM&qcapi*

*https://gdscartao.com.br/unisabor?querycard=16150001503&pin=N6E4F7KIC6&qcapi*

*https://gdscartao.com.br/unisabor?querycard=16150004109&pin=AQW55LDRUW&qcapi*

*https://gdscartao.com.br/unisabor?querycard=16150005000&pin=O3K4U8I8E9&qcapi*

*https://gdscartao.com.br/unisabor?querycard=16150006713&pin=R67BGZURPY&qcapi*


Essa api irá retornar os dados de codigo do cartão, nome do cliente, nome da empresa, saldo, data de ultima atualização e uma array de movimentos do cartao contendo os itens vendidos da seguinte forma: id da operacao, codigo do produto, descrição do produto data de ocorrencia, quantidade vendida, valor total da venda do item e se o item foi cancelado ou não.

O aplicativo deve exibir a consulta com os dados retornados mostrando o nome da empresa, nome do cliente e saldo do cartao e abaixo ou pelo acesso por algum botão os itens da movimentação exibidos em lista.

Os dados de cada consulta devem ser salvos no banco de dados local e exibir um botão para que o usuário possa visualizar as ultimas consultas realizadas em forma de lista e ao clicar no item da lista também exibir os dados de movimentação. 

Salvar no banco de dados somente as últimas 15 consultas.

A solução para o aplicativo deverá utilizar Android Studio e Material Design.
baseUrl é https://gdscartao.com.br/


